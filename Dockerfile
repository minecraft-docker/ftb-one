FROM alpine
WORKDIR /opt/minecraft

RUN apk add --no-cache bash
RUN apk add --no-cache py-pip
RUN pip install mcstatus

RUN wget -O serverinstaller "https://api.modpacks.ch/public/modpack/97/6482/server/linux"
RUN chmod +x serverinstaller
RUN ./serverinstaller 97 6482 --auto
RUN rm serverinstaller

RUN echo "eula=true" > eula.txt


HEALTHCHECK --interval=30s --timeout=5s --retries=5 \
   CMD mcstatus localhost ping || exit 1

EXPOSE 25565
ENTRYPOINT ["/opt/minecraft/start.sh"]
